import java.util.InputMismatchException;
import java.util.Scanner;

public class Noten {
public static void main(String[] args) {
	int note = eingabe();
	ausgabe(note);
}

private static void ausgabe(int note) {
	switch(note) {
	case 1:
		System.out.println(note + " = Sehr gut");
		break;
	case 2:
		System.out.println(note + " = Gut");
		break;
	case 3:
		System.out.println(note + " = Befriedigend");
		break;
	case 4:
		System.out.println(note + " = Ausreichend");
		break;
	case 5:
		System.out.println(note + " = Mangelhaft");
		break;
	case 6:
		System.out.println(note + " = Ungenügend");
		break;
	default:
		System.out.println("Fehler: Diese Note existiert nicht.");
	}
	
}

private static int eingabe() {
	Scanner sc = new Scanner(System.in);
	int note = 0;
	while(note == 0) {
		try {
			System.out.println("Bitte geben Sie ihre Note ein.");
			note = sc.nextInt();
			return note;
			
		}catch(InputMismatchException e) {
			System.out.println("Falsche Eingabe.");
			sc.nextLine();
			
		}
	}return 0;
}
}
