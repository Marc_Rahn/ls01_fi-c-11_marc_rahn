import java.util.InputMismatchException;
import java.util.Scanner;

public class CelsiusToFahrenheit {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double startWert = getDouble(sc, "Bitte den Startwert in Celsius eingeben: ");
		double zwischenWert = startWert;
		double endWert = 0;
		endWert = getDouble(sc, "Bitte den Endwert in Celsius eingeben: ");
		double schrittweite = getDouble(sc, "Bitte die Schrittweite in Grad Celsius eingeben: ");
		formatierung(startWert, zwischenWert, endWert, schrittweite);
	}

	private static void formatierung(double startWert, double zwischenWert, double endWert, double schrittweite) {
		Double fahrenheit = null;
		if (startWert <= endWert) {
			while (zwischenWert <= endWert) {
				fahrenheit = umrechnung(zwischenWert);
				System.out.printf("%7.2f�C%15.2f�F\n", zwischenWert, fahrenheit);
				zwischenWert += schrittweite;
			}
			return;
		}
		if (startWert >= endWert) {
			while (zwischenWert >= endWert) {
				fahrenheit = umrechnung(zwischenWert);
				System.out.printf("%7.2f�C%15.2f�F\n", zwischenWert, fahrenheit);
				zwischenWert -= schrittweite;
			}
			return;
		}
	}

	private static double umrechnung(double zwischenWert) {
		double fahrenheit = (zwischenWert * 1.8 + 32);
		return fahrenheit;
	}

	private static Double getDouble(Scanner sc, String text) {
		Double input = null;
		while (input == null) {
			try {
				System.out.println(text);
				input = sc.nextDouble();
			} catch (InputMismatchException e) {
				sc.nextLine();
				System.out.println("Invalid input. \n");
			}
		}
		return input;
	}
}
