import java.util.InputMismatchException;
import java.util.Scanner;

public class CreateSquare {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Byte input = null;
		input = getInput(sc, input);
		String length = "";
		logicOfSquare(input, length);
	}

	private static void logicOfSquare(Byte input, String length) {
		for (int i = 0; i < input; i++) {
			if (i == 0 || i == (input - 1)) {
				length = edgeLength(input, length);
				System.out.println(length);
			} else {
				length = midLenght(input, length);
				System.out.println(length);
			}
		}
	}

	private static String edgeLength(Byte input, String length) {
		length = "";
		for (int j = 0; j < input; j++) {
			length += "* ";
		}
		return length;
	}

	private static String midLenght(Byte input, String length) {
		length = "";
		length += "* ";
		for (int f = 0; f < input - 2; f++) {
			length += "  ";
		}
		length += "* ";
		return length;
	}

	private static Byte getInput(Scanner sc, Byte input) {
		while (input == null) {
			try {
				System.out.println("Please provide the desired side-length.");
				input = sc.nextByte();
			} catch (InputMismatchException e) {
				sc.nextLine();
				System.out.println("Invalid input. \n");
			}
		}
		return input;
	}

}
