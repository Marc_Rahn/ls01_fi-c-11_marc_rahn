import java.util.Arrays;

public class Array_sort {

	public static void main(String[] args) {
		//Aufgabe 3 "Arrays mit Methoden"
		
		int[] intArray = {0,1,2,3,4,5,6,7,8,9};
		int[] reverseIntArray = new int[intArray.length];
		int j = 0;
		reverseArray(intArray, reverseIntArray, j);
	}

	private static int[] reverseArray(int[] intArray, int[] reverseIntArray, int j) {
		for(int i=intArray.length-1; i>0; i--) {
			reverseIntArray[j] = intArray[i];
			j++;
		}
		System.out.println(Arrays.toString(reverseIntArray));
		return reverseIntArray;
	}

}